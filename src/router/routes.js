const routes = [
  {
    path: '/',
    redirect: '/menu'
  },
  {
    path: '/menu',
    component: () => import('@/components/PluginTabsContainer.vue'),
    children: [
      {
        path: '',
        redirect: 'marketing'
      },
      {
        path: 'marketing',
        name: 'marketing',
        component: () => import('@/components/TabContent.vue')
      },
      {
        path: 'finance',
        name: 'finance',
        component: () => import('@/components/TabContent.vue')
      },
      {
        path: 'personnel',
        name: 'personnel',
        component: () => import('@/components/TabContent.vue')
      }
    ]
  }
]
export default routes
